#!/usr/bin/env bash
javac -cp src/ src/admix/AdmixMain.java
jar cfe flare.jar admix/AdmixMain -C src/ ./
jar -i flare.jar
mkdir -p ${PREFIX}/opt/flare-0.2.0
cp flare.jar ${PREFIX}/opt/flare-0.2.0
