#!/bin/bash
sed 's/os.path.join(os.path.dirname(__file__), "data")/os.path.join(os.path.dirname(__file__), "..\/share\/cyrius\/data")/' star_caller.py > star_caller.py.bak && mv star_caller.py.bak star_caller.py
cp $RECIPE_DIR/setup.py ./
$PYTHON setup.py install
mkdir -p $PREFIX/share/cyrius
cp -R data $PREFIX/share/cyrius
