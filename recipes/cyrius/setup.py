#!/usr/bin/env python

from distutils.core import setup

setup(name='Cyrius',
        version='1.1.1',
        description='A tool to genotype CYP2D6 with WGS data',
        author='Xiao Chen',
        url='https://github.com/Illumina/Cyrius',
        packages=['caller', 'depth_calling'],
        scripts=['star_caller.py']
        )
