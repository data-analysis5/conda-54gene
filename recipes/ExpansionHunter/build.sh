#!/bin/bash
sed -i 's/Boost_USE_STATIC_LIBS ON/Boost_USE_STATIC_LIBS OFF/ ; s/absl::flat_hash_set/absl::flat_hash_set -ldl -ldeflate/ ; s/target_link_libraries(ExpansionHunter ExpansionHunterLib/target_link_libraries(ExpansionHunter ExpansionHunterLib -ldl -ldeflate/' ehunter/CMakeLists.txt
sed -i 's/#include <memory>/#include <memory>\n#include <limits>/' ehunter/thirdparty/graph-tools-master-f421f4c/include/graphcore/GraphCoordinates.hh
cat <(echo -e '#pragma GCC diagnostic push\n#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"') ehunter/locus/RepeatAnalyzer.cpp <(echo -e '#pragma GCC diagnostic pop') > ehunter/locus/RepeatAnalyzer.cpp.tmp && mv ehunter/locus/RepeatAnalyzer.cpp.tmp ehunter/locus/RepeatAnalyzer.cpp
cp $RECIPE_DIR/CMakeLists.txt .
mkdir build
cd build
cmake ..
make
mkdir -p $PREFIX/bin
cp install/bin/ExpansionHunter $PREFIX/bin
mkdir -p $PREFIX/share/ExpansionHunter/variant_catalog
cp ../variant_catalog/hg38/variant_catalog.json $PREFIX/share/ExpansionHunter/variant_catalog/variant_catalog_GRCh38.json
