# Install eagle using make/make install processing chain
# note that HTSLIB_DIR is used in a non-standard fashion in the makefile,
# so this is just a placeholder that doesn't do anything, and the actual
# functions of the variable are really handled by BOOST_INSTALL_DIR
cd src
make CC=$CXX BLAS_DIR=$PREFIX/lib BOOST_INSTALL_DIR=$PREFIX HTSLIB_DIR=$PREFIX
# Install eagle binary
mkdir -p $PREFIX/bin
cp eagle $PREFIX/bin/eagle
# Install eagle supplementary files
mkdir -p $PREFIX/share/eagle/tables
cd ..
cp tables/*38* $PREFIX/share/eagle/tables
