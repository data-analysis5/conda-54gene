cget install -f requirements.txt
mkdir build && cd build
cmake -DCMAKE_TOOLCHAIN_FILE=../cget/cget/cget.cmake ..
make
mkdir -p $PREFIX/bin/
cp minimac4 $PREFIX/bin/
