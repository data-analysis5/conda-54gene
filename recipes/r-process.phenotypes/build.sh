#!/bin/bash
PV="1.4.0"
PTAG="${PV}"
tar xzvf process.phenotypes-${PTAG}.tar.gz
cd process.phenotypes-${PTAG}
$R CMD build .
$R CMD INSTALL --example --html process.phenotypes_${PV}.tar.gz
