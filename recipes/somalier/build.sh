#!/bin/bash
mkdir -p $PREFIX/share/somalier
wget -O $PREFIX/share/somalier/sites.hg38.vcf.gz https://github.com/brentp/somalier/files/3412456/sites.hg38.vcf.gz
mkdir -p $PREFIX/bin
cp somalier $PREFIX/bin
chmod ugo+x $PREFIX/bin/somalier
