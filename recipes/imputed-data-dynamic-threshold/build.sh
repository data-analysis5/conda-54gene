#!/usr/bin/env bash
IDDT_VERSION=$(ls imputed-data-dynamic-threshold*tar.gz | sed 's/imputed-data-dynamic-threshold-// ; s/.tar.gz//')
tar zxvf imputed-data-dynamic-threshold-${IDDT_VERSION}.tar.gz
cd imputed-data-dynamic-threshold-${IDDT_VERSION}
autoreconf --force --install
./configure --prefix=$PREFIX --with-boost=$PREFIX --with-boost-libdir=$PREFIX/lib
make imputed-data-dynamic-threshold.out
/usr/bin/install -c imputed-data-dynamic-threshold.out $PREFIX/bin/imputed-data-dynamic-threshold.out
