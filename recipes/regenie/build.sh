#!/bin/bash

$BUILD_PREFIX/bin/sed -i 's/g++/\$(CXX)/ ; s/gfortran/\$(FC)/ ; s/gcc/\$(CC)/' external_libs/mvtnorm/Makefile
$BUILD_PREFIX/bin/sed -i 's/-lboost$/-lboost_filesystem/' Makefile
$BUILD_PREFIX/bin/sed -i '1 i\#include <memory>' src/*.hpp
#$BUILD_PREFIX/bin/sed -E -i 's/([^ \)])>>/\1> >/g' external_libs/pgenlib/pgenlibr.h src/*pp
$BUILD_PREFIX/bin/make BGEN_PATH=$PREFIX/lib HAS_BOOST_IOSTREAM=1 OPENBLAS_ROOT=$PREFIX/lib CXXFLAGS="-O3 -std=c++11 -I$BUILD_PREFIX/include -I$PREFIX/include -L$BUILD_PREFIX/lib -L$PREFIX/lib"
mkdir -p $PREFIX/bin
cp regenie $PREFIX/bin
