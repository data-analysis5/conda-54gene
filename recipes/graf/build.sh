#!/usr/bin/env bash
cd graf-master
mkdir $PREFIX/bin
cp bin/graf bin/graf_dups bin/PlotPopulations.pl bin/PlotGraf.pl $PREFIX/bin
mkdir $PREFIX/share
mkdir $PREFIX/share/graf
cp -R data/* $PREFIX/share/graf
