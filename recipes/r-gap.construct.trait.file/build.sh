#!/bin/bash
PV="1.0.7"
PTAG="${PV}"
tar xzvf gap.construct.trait.file-${PTAG}.tar.gz
cd gap.construct.trait.file-${PTAG}
$R CMD INSTALL .
